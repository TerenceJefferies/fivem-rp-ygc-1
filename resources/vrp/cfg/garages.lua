
local cfg = {}
-- define garage types with their associated vehicles
-- (vehicle list: https://wiki.fivem.net/wiki/Vehicles)

-- each garage type is an associated list of veh_name/veh_definition
-- they need a _config property to define the blip and the vehicle type for the garage (each vtype allow one vehicle to be spawned at a time, the default vtype is "default")
-- this is used to let the player spawn a boat AND a car at the same time for example, and only despawn it in the correct garage
-- _config: vtype, blipid, blipcolor, permissions (optional, only users with the permission will have access to the shop)

cfg.rent_factor = 0.02 -- 10% of the original price if a rent
cfg.job_rent_factor = 0.005
cfg.sell_factor = 0.55 -- sell for 75% of the original price

cfg.garage_types = {
  ["police"] = {
    _config = {vtype="car",blipid=56,blipcolor=38,permissions={"police.vehicle"}},
    ["sheriff"] = {"Sheriff Cruiser",0, ""},
    ["sheriff2"] = {"Sheriff SUV",0, ""},
    ["police4"] = {"Unmarked Cruiser",0, ""},
    ["FBI"] = {"Unmarked Cruiser (Buffalo)",0, ""},
    ["FBI2"] = {"Unmarked SUV",0, ""},
    ["policeb"] = {"Police Bike",0, ""}
  },
  ["EMS/Fire Depot"] = {
    _config = {vtype="car",blipid=50,blipcolor=3,permissions={"emergency.vehicle"}},
    ["Ambulance"] = {"Ambulance",0, "emergency"},
	  ["firetruck"] = {"firetruk",0, "emergency"}
  },
  ["Bank Security Vehicles"] = {
    _config = {vtype="car",blipid=50,blipcolor=3,permissions={"bank.vehicle"}},
    ["Dilettante2"] = {"Dilettante Security",0, "For use when performing security duties."},
    ["Stockade"] = {"Stockade",0, "For use in cash transfer jobs."}
  },
  ["GSFR Vehicles"] = {
    _config = {vtype="car",blipid=50,blipcolor=3,permissions={"bobcat.vehicle"}},
    ["Dilettante2"] = {"Dilettante Security",0, "For use when performing security duties."}
  },
  ["Police Helicopters"] = {
    _config = {vtype="car",blipid=43,blipcolor=38,radius=5.1,permissions={"police.vehicle"}},
    ["polmav"] = {"Maverick",0, "emergency"}
  },
   ["EMS Helicopters"] = {
    _config = {vtype="car",blipid=43,blipcolor=1,radius=5.1,permissions={"emergency.vehicle"}},
    ["supervolito2"] = {"EMS",0, "emergency"}
  },
  --
  -- CUSTOM GARAGES
  ["Jerrys Budget Autos"] = {
    _config = {vtype="car",blipid=225,blipcolor=52},
    ["dilettante"] = {"Dilettante",9000, ""},
    ["kalahari"] = {"Kalahari",9000, ""},
    ["tractor"] = {"Classic Tractor",7500, ""},
    ["voodoo"] = {"Voodoo",10000, ""},
    ["journey"] = {"Journey",10000, ""},
    ["emperor"] = {"Emperor",10000, ""},
    ["manana"] = {"Manana",12000, ""},
    ["ingot"] = {"Ingot",12000, ""},
    ["glendale"] = {"Glendale",12000, ""},
    ["asea"] = {"Asea",12000, ""},
    ["faction"] = {"Faction",12000, ""},
    ["bodhi2"] = {"Bodhi",12000, ""},
    ["rebel"] = {"Rebel",12000, ""},
    ["rebel2"] = {"Rebel Plus",12500, ""},
    ["pony"] = {"Pony",12000, ""},
    ["bfinjection"] = {"BF-Injection",15000, ""},
    ["futo"] = {"Futo",15000, ""},
    ["stalion"] = {"Stalion",15000, ""},
    ["issi3"] = {"Classic Issi",15000, ""},
    ["phoenix"] = {"Phoenix",15000, ""},
    ["saabregt"] = {"Saabre GT",17000, ""},
	  ["fagaloa"] = {"Fagaloa",20000, ""},
    ["mesa"] = {"Mesa",20000, ""},
    ["mesa3"] = {"Lifted Mesa",30000, ""},
    ["rancherxl"] = {"Rancher XL",20000, ""},
    ["landstalker"] = {"Landstalker",25000, ""},
    ["granger"] = {"granger",25000, ""}
  },

  ["Euro Autos Paleto"] = {
    _config = {vtype="car",blipid=225,blipcolor=52},
    ["oracle"] = {"Oracle",20000, ""},
    ["brioso"] = {"Brioso",30000, ""},
    ["felon"] = {"Felon",30000, ""},
    ["felon2"] = {"Felon Cabriolet",30000, ""},
    ["schafter2"] = {"Schafter",35000, ""},
    ["sentinel3"] = {"Classic Sentinel",35000, ""},
    ["windsor"] = {"Windsor",50000, ""},
    ["windsor2"] = {"Windsor Drop Top",52500, ""},
    ["rapidgt"] = {"Rapid-GT",50000, ""},
    ["raptor"] = {"Raptor",75000, ""},
    ["gb200"] = {"GB-200",125000, ""},
    ["ruston"] = {"Ruston",150000, ""},
    ["sultanrs"] = {"SultanRS",150000, ""},
    ["gp1"] = {"GP-1",350000, ""},
    ["cheetah"] = {"Cheetah",350000, ""},
    ["osiris"] = {"Osiris",350000, ""}
  },

  ["Paranoia Automotive"] = {
    _config = {vtype="car",blipid=225,blipcolor=52},
    ["crusader"] = {"Crusader",15000, ""},
    ["issi4"] = {"Classic Issi Offroad",15000, ""},
    ["xls2"] = {"XLS Armoured",50000, ""},
    ["cognoscenti2"] = {"Cognoscenti Armoured",55000, ""},
    ["freecrawler"] = {"Freecrawler",50000, ""},
    ["brawler"] = {"Brawler",50000, ""},
    ["sandking"] = {"Sandking",80000, ""},
    ["dubsta3"] = {"Dubsta 6x6",80000, ""},
    ["brutus"] = {"Brutus",80000, ""},
    ["insurgent2"] = {"Insurgent",125000, ""},
  },

  ["Lost Bikes"] = {
    _config = {vtype="bike",blipid=226,blipcolor=25},
    ["faggio"] = {"faggio",2500, ""},
    ["faggio2"] = {"faggio classic",2500, ""},
    ["wolfsbane"] = {"wolfsbane",10000, ""},
    ["sanchez"] = {"Sanchez",10000, ""},
    ["pcj"] = {"PCJ 600",10000, ""},
    ["akuma"] = {"Akuma",12500, ""},
    ["vindicator"] = {"Vindicator",12500, ""},
    ["vortex"] = {"Vortex",12500, ""},
    ["sanctus"] = {"Sanctus",12500, ""},
    ["fcr"] = {"FCR",12500, ""},
  },
  ["Taxi Depot"] = {
    _config = {vtype="car",blipid=198,blipcolor=81,permissions={"taxi.vehicle"}},
    ["taxi"] = {"Taxi",2000, "A marked taxi cab!"},
    ["surge"] = {"Surge",3000, "Surge electric car - perfect as a discreet taxi."},
    ["RentalBus"] = {"Rental Bus",5000, "A short executive minibus, capable of carrying more passengers than a standard cab."},
  },
  ["Mechanic Depot"] = {
    _config = {vtype="car",blipid=68,blipcolor=31,permissions={"repair.vehicle"}},
    ["utillitruck3"] = {"Utility Truck",3500, "A standard utility truck to store your tools!"},
    ["TowTruck2"] = {"Tow Truck",4000, "A small tow truck."}
  },
  ["Courier Depot"] = {
    _config = {vtype="car",blipid=478,blipcolor=31,permissions={"trucker.vehicle"}},
    ["mule"] = {"Mule",7500, "A small delivery truck"},
    ["benson"] = {"Benson",10000, "A mid-sized delivery truck."},
    ["pounder"] = {"Pounder",15000, "A large delivery truck, able to carry twice what a mule can."}
  },
  ["Food Truck Depot"] = {
    _config = {vtype="car",blipid=88,blipcolor=31,permissions={"ftd.vehicle"}},
    ["taco"] = {"Taco Truck",2000, "A fully equipped food truck ready for food service!"}
  },
}

-- {garage_type,x,y,z}
cfg.garages = {
  {"Jerrys Budget Autos",73.799415588379,6636.2729492188,31.434194564819},
  {"Euro Autos Paleto",-211.07650756836,6195.8920898438,30.997495651245},
  {"Paranoia Automotive",2930.8237304688,4631.677734375,48.055870056152},
  {"Lost Bikes",1972.5239257813,4641.4682617188,40.971591949463},
  --Job Depots
  {"Taxi Depot",-276.91021728516,6169.828125,31.500904083252},
  {"Mechanic Depot",1694.5532226563,6428.3940429688,32.630062103271},
  {"Courier Depot",-95.353370666504,6493.4697265625,31.490900039673},
  {"Food Truck Depot",195.07902526855,6383.5209960938,31.390600204468},
  {"police",-476.92425537109,6026.9951171875,31.340547561646},
  {"EMS/Fire Depot",-356.94149780273,6111.2514648438,31.440160751343},
  {"Bank Security Vehicles",-120.89163208008,6486.7412109375,31.468461990356},
  {"GSFR Vehicles",2907.9460449219,4467.62890625,48.205478668213}
}

return cfg
