
local cfg = {}

-- PCs positions
cfg.pcs = {
  {-448.97076416016,6012.4208984375,31.71639251709}
}

-- vehicle tracking configuration
cfg.trackveh = {
  min_time = 300, -- min time in seconds
  max_time = 600, -- max time in seconds
  service = "Sheriffs Deputy",  -- service to alert when the tracking is successful
  "Sheriff",
}

-- wanted display
cfg.wanted = {
  blipid = 458,
  blipcolor = 38,
  service = "police"
}

-- illegal items (seize)
cfg.seizable_items = {
  "dirty_money",
  "cocaine",
  "lsd",
  "seeds",
  "harness",
  "credit",
  "weed",
  "fake_id",
  "driver",
  "akreceiver",
  "akbarrel",
  "aktrigger",
  "akcomponents",
  "mpbody",
  "mptrigger",
  "rpgshell",
  "rpgtube",
  "rpgreceiver",
  "rpgtrigger",
  "explosiveord"
}

-- jails {x,y,z,radius}
cfg.jails = {
  {-311.59701538086,6105.9111328125,31.498811721802,2.1}
}

-- fines
-- map of name -> money
cfg.fines = {
  ["Public Intoxication"] = 50,
  ["Disorderly Conduct"] = 100,
  ["Speeding"] = 150,
  ["Driving Without a License"] = 250,
  ["Reckless Driving"] = 250,
  ["Theft"] = 500,
  ["Possession of Drugs"] = 250,
  ["Possession of Weapon Parts"] = 500,
  ["Possession of Dirty Money"] = 500
}

return cfg
