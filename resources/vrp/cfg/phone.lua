
local cfg = {}

-- size of the sms history
cfg.sms_history = 15

-- maximum size of an sms
cfg.sms_size = 500

-- duration of a sms position marker (in seconds)
cfg.smspos_duration = 300

-- define phone services
-- blipid, blipcolor (customize alert blip)
-- alert_time (alert blip display duration in seconds)
-- alert_permission (permission required to receive the alert)
-- alert_notify (notification received when an alert is sent)
-- notify (notification when sending an alert)
cfg.services = {
  ["police"] = {
    blipid = 304,
    blipcolor = 38,
    alert_time = 30, -- 5 minutes
    alert_permission = "police.service",
    alert_notify = "~r~911 Call:~n~~s~",
    notify = "~b~You called the police.",
    answer_notify = "~b~The police are coming."
  },
  ["emergency"] = {
    blipid = 153,
    blipcolor = 1,
    alert_time = 30, -- 5 minutes
    alert_permission = "emergency.service",
    alert_notify = "~r~Emergency alert:~n~~s~",
    notify = "~b~You called for EMS/Fire!",
    answer_notify = "~b~The fire Department are coming."
  },
  ["PB Taxi Co."] = {
    blipid = 198,
    blipcolor = 5,
    alert_time = 300,
    alert_permission = "taxi.service",
    alert_notify = "~y~PB Taxi Co. - Driver Requested!:~n~~s~",
    notify = "~y~You called a taxi, you'll be alerted if it's accepted by a driver.",
    answer_notify = "~y~Your Taxi is coming."
  },
  ["Mechanic"] = {
    blipid = 446,
    blipcolor = 5,
    alert_time = 300,
    alert_permission = "repair.service",
    alert_notify = "~y~Repair alert:~n~~s~",
    notify = "~y~You called a Mechanic. You'll be alerted if it's accepted by a Mechanic",
    answer_notify = "~y~A Mechanic is coming!"
  },
  ["Courier"] = {
    blipid = 478,
    blipcolor = 5,
    alert_time = 300,
    alert_permission = "courier.service",
    alert_notify = "~y~JOB ALERT:~n~~s~",
    notify = "~y~You have asked for a delivery, you'll be notified when it's on it's way.",
    answer_notify = "~y~Your Delivery is on it's way!"
  },
  ["Bobcat Response Security"] = {
    blipid = 478,
    blipcolor = 5,
    alert_time = 300,
    alert_permission = "security.service",
    alert_notify = "~y~Security Alert:~n~~s~",
    notify = "~y~Your security request has been sent. You will be alerted if it is accepted.",
    answer_notify = "~y~A BOBCAT RESPONSE OFFICER IS EN-ROUTE!"
  }
}

-- define phone announces
-- image: background image for the announce (800x150 px)
-- price: amount to pay to post the announce
-- description (optional)
-- permission (optional): permission required to post the announce
cfg.announces = {
  ["admin"] = {
    --image = "nui://vrp_mod/announce_admin.png",
    image = "http://i.imgur.com/kjDVoI6.png",
    price = 0,
    description = "Admin only.",
    permission = "admin.announce"
  },
  ["police"] = {
    --image = "nui://vrp_mod/announce_police.png",
    image = "http://i.imgur.com/DY6DEeV.png",
    price = 0,
    description = "Only for police, ex: wanted advert.",
    permission = "police.announce"
  },
  ["commercial"] = {
    --image = "nui://vrp_mod/announce_commercial.png",
    image = "http://i.imgur.com/b2O9WMa.png",
    description = "Commercial stuff (buy, sell, work).",
    price = 250
  }
}

return cfg
