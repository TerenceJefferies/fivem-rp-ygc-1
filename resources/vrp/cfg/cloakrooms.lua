
-- this file configure the cloakrooms on the map

local cfg = {}

-- prepare surgeries customizations
local surgery_male = { model = "mp_m_freemode_01" }
local surgery_female = { model = "mp_f_freemode_01" }
local emergency_male = { model = "s_m_m_paramedic_01" }
local emergency_female = { model = "s_f_y_paramedic_01" }
local fbi_male = { model = "s_m_y_swat_01" }
local sheriff_male = { model = "s_m_y_sheriff_01"}
local sheriff_female = { model = "s_f_y_sheriff_01"}
local hway_male = { model = "s_m_y_hwaycop_01"}
local cop_male = { model = "s_m_y_cop_01"}
local ups_male = { model = "s_m_m_ups_02"}
local cop_female = { model = "s_f_y_cop_01"}
local detective_male = { model = "s_m_m_CIASec_01"}
local officer_male = { model = "s_m_y_cop_01"}
local bounty_male = { model = "s_m_y_BlackOps_01"}
local captain_male = { model = "s_m_y_fibcop_01"}
local lieutenant_male = { model = "s_m_m_Armoured_02"}
local sergeant_male = { model = "s_m_y_Ranger_01"}
local deputy_male = { model = "s_m_y_ranger_01"}
local chief_male = {model = "s_m_m_ciasec_01"}
local santa = {model = "Santaclaus"}
local securityGen = {model = "s_m_m_security_01"}
local prisonGuard = {model = "s_m_m_prisguard_01"}
local securityArmoured = {model = "s_m_m_Armoured_01"}
local securityArmoured2 = {model = "s_m_m_Armoured_02"}
local securityBlackArmour = {model = "s_m_m_ChemSec_01"}
local suitSecurity1 = {model = "s_m_m_HighSec_01"}
local suitSecurity2 = {model = "s_m_m_HighSec_02"}
local suitSecurity3 = {model = "JewelSec_01"}

--s_m_m_paramedic_01
--s_f_y_scrubs_01
--mp_m_freemode_01
--mp_f_freemode_01


for i=0,19 do
  surgery_female[i] = {0,0}
  surgery_male[i] = {0,0}
end

-- cloakroom types (_config, map of name => customization)
--- _config:
---- permissions (optional)
---- not_uniform (optional): if true, the cloakroom will take effect directly on the player, not as a uniform you can remove
cfg.cloakroom_types = {
  ["police"] = {
    _config = { permissions = {"police.cloakroom"} },
    ["Male uniform"] = {
      [3] = {30,0},
      [4] = {25,2},
      [6] = {24,0},
      [8] = {58,0},
      [11] = {55,0},
      ["p2"] = {2,0}
    },
    ["Female uniform"] = {
      [3] = {35,0},
      [4] = {30,0},
      [6] = {24,0},
      [8] = {6,0},
      [11] = {48,0},
      ["p2"] = {2,0}
    }
  },
  ["Lawyer"] = {
    _config = { permissions = {"Lawyer.cloakroom"} },
    ["Male uniform"] = {
	  [3] = {1,0},
	  [4] = {10,0},
	  [6] = {10,0},
	  [8] = {4,0},
	  [11] = {10,0},
	  ["p2"] = {-1,0}
    },
    ["Female uniform"] = {
	  [3] = {0,0},
	  [4] = {37,0},
	  [6] = {13,0},
	  [8] = {21,1},
	  [11] = {24,3},
	  ["p2"] = {-1,0}
    }
},
  ["surgery"] = {
    _config = { not_uniform = true },
    ["Male"] = surgery_male,
    ["Female"] = surgery_female
  },
   -- ["Santa"] = {
    -- _config = { permissions = {"santa.cloakroom"} },
    -- ["Santa Outfit"] = santa
    -- },
   ["emergency"] = {
    _config = { permissions = {"emergency.cloakroom"} },
    ["Male"] = emergency_male,
    ["Female"] = emergency_female
    },
  ["Bounty"] = {
    _config = { permissions = {"Bounty.cloakroom"} },
    ["Bounty"] = bounty_male
  },
  ["Bank Security Cloakroom"] = {
    _config = { permissions = {"security.cloakroom"} },
    ["Bank Sec Male 1"] = suitSecurity1,
    ["Bank Sec Male 2"] = suitSecurity2,
    ["Bank Sec Male 3"] = suitSecurity3,
    ["Security Armoured (Black)"] = securityBlackArmour
  },
  ["Bobcat Response Cloakroom"] = {
    _config = { permissions = {"security.cloakroom"} },
    ["Security Male 1"] = securityGen,
    ["Security Armoured Male 1"] = securityArmoured,
    ["Security Armoured Male 2"] = securityArmoured2,
    ["Security Armoured (Black)"] = securityBlackArmour
  },
  ["sheriff"] = {
    _config = { permissions = {"sheriff.cloakroom"} },
    ["Standard Patrol (M)"] = sheriff_male,
    ["Standard Patrol (F)"] = sheriff_female,
    ["Alt Patrol (M)"] = deputy_male,
    ["Specialist (M)"] = fbi_male,
    ["Plain Clothes (M)"] = detective_male,
    ["Trooper (M)"] = hway_male
  }
}

cfg.cloakrooms = {
  {"sheriff", -447.56564331055,6007.9501953125,31.716371536255},
  {"emergency",-371.05349731445,6101.4780273438,31.493085861206},
  {"Bank Security Cloakroom",-102.67510223389,6465.6489257813,31.626703262329},
  {"Bobcat Response Cloakroom",2905.2084960938,4484.4467773438,48.102245330811}
  -- {"Santa",-1373.0778808594,-2677.6694335938,13.944942474365} -- Santa's Cloakroom (Disabled)
}

return cfg
