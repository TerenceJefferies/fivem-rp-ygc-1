
local items = {}

items["tkit"] = {"Trauma Medical Kit","A professional medical kit carried by EMS to help seriously injured people.",nil,1}
items["fakit"] = {"First Aid Kit","A small medical kit that can be used to treat seriously injured people.",nil,0.7}
items["dirty_money"] = {"Dirty money","Illegally earned money.",nil,0}
items["parcels"] = {"Parcels","Parcels to deliver",nil,0.10}
items["repairkit"] = {"Repair Kit","Used to repair vehicles.",nil,0.5}
--Food Truck Raw items
items["tacoshells"] = {"Taco Shells","A pack of Taco Shells",nil,0.1}
items["mincemeat"] = {"Beef Mincemeat","Premium quality mincemeat",nil,0.1}
items["mexbeans"] = {"Mexican Style Beans","These beans are from San Andreas, but they think they are mexican,",nil,0.1}
items["tortilla"] = {"Tortillas","A pack of tortillas",nil,0.1}
items["salsa"] = {"Salsa","Salsa made from a secret recipe",nil,0.1}
items["spicysalsa"] = {"Spicy Salsa","Salsa made from a secret super spicy recipe",nil,0.1}
items["batter"] = {"Batter","Sweet Batter used for donuts",nil,0.1}
items["sugar"] = {"Sugar","A box of sugar",nil,0.1}
items["rawfries"] = {"Uncooked Fries","Fries ready for frying!",nil,0.1}
--Weapon Smuggler items
items["akreceiver"] = {"AK47 Receiver","The receiver body of an AK, used to assemble one.",nil,0.5}
items["akbarrel"] = {"AK47 Barrel","The barrel of a AK47",nil,0.2}
items["aktrigger"] = {"AK47 Trigger Body","The trigger body of an AK47",nil,0.2}
items["akcomponents"] = {"AK47 Components","Various required components of an AK47.",nil,0.5}
items["mpbody"] = {"Machine Pistol Body","The entire body of a machine pistol, including barrel.",nil,0.5}
items["mptrigger"] = {"Machine Pistol Trigger Body","The trigger body of a machine pistol.",nil,0.2}
items["rpgshell"] = {"RPG Shell","A deactivated RPG Shell.",nil,0.5}
items["rpgtube"] = {"RPG Tube","A tube to an RPG",nil,1}
items["rpgreceiver"] = {"RPG Receiver","The receiver body of an RPG",nil,0.5}
items["rpgtrigger"] = {"RPG Trigger Body","The trigger body for an RPG.",nil,0.2}
items["explosiveord"] = {"Explosive Ordnance","A stripped down grenade for assembly into an RPG rocket.",nil,1}
--Banking
items["cashbox"] = {"Banking Cash Box","A security box used to store money for transit.",nil,5}


-- money
items["money"] = {"Money","Packed money.",function(args)
  local choices = {}
  local idname = args[1]

  choices["Unpack"] = {function(player,choice,mod)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      local amount = vRP.getInventoryItemAmount(user_id, idname)
      vRP.prompt(player, "How much to unpack ? (max "..amount..")", "", function(player,ramount)
        ramount = parseInt(ramount)
        if vRP.tryGetInventoryItem(user_id, idname, ramount, true) then -- unpack the money
          vRP.giveMoney(user_id, ramount)
          vRP.closeMenu(player)
        end
      end)
    end
  end}

  return choices
end,0}

-- money binder
items["money_binder"] = {"Money binder","Used to bind 1000$ of money.",function(args)
  local choices = {}
  local idname = args[1]

  choices["Bind money"] = {function(player,choice,mod) -- bind the money
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      local money = vRP.getMoney(user_id)
      if money >= 1000 then
        if vRP.tryGetInventoryItem(user_id, idname, 1, true) and vRP.tryPayment(user_id,1000) then
          vRP.giveInventoryItem(user_id, "money", 1000, true)
          vRP.closeMenu(player)
        end
      else
        vRPclient.notify(player,{vRP.lang.money.not_enough()})
      end
    end
  end}

  return choices
end,0}

-- parametric weapon items
-- give "wbody|WEAPON_PISTOL" and "wammo|WEAPON_PISTOL" to have pistol body and pistol bullets

local get_wname = function(weapon_id)
  local name = string.gsub(weapon_id,"WEAPON_","")
  name = string.upper(string.sub(name,1,1))..string.lower(string.sub(name,2))
  return name
end

--- weapon body
local wbody_name = function(args)
  return get_wname(args[2]).." body"
end

local wbody_desc = function(args)
  return ""
end

local wbody_choices = function(args)
  local choices = {}
  local fullidname = joinStrings(args,"|")

  choices["Equip"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      if vRP.tryGetInventoryItem(user_id, fullidname, 1, true) then -- give weapon body
        local weapons = {}
        weapons[args[2]] = {ammo = 0}
        vRPclient.giveWeapons(player, {weapons})

        vRP.closeMenu(player)
      end
    end
  end}

  return choices
end

local wbody_weight = function(args)
  return 0.75
end

items["wbody"] = {wbody_name,wbody_desc,wbody_choices,wbody_weight}

--- weapon ammo
local wammo_name = function(args)
  return get_wname(args[2]).." ammo"
end

local wammo_desc = function(args)
  return ""
end

local wammo_choices = function(args)
  local choices = {}
  local fullidname = joinStrings(args,"|")

  choices["Load"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      local amount = vRP.getInventoryItemAmount(user_id, fullidname)
      vRP.prompt(player, "Amount to load ? (max "..amount..")", "", function(player,ramount)
        ramount = parseInt(ramount)

        vRPclient.getWeapons(player, {}, function(uweapons)
          if uweapons[args[2]] ~= nil then -- check if the weapon is equiped
            if vRP.tryGetInventoryItem(user_id, fullidname, ramount, true) then -- give weapon ammo
              local weapons = {}
              weapons[args[2]] = {ammo = ramount}
              vRPclient.giveWeapons(player, {weapons,false})
              vRP.closeMenu(player)
            end
          end
        end)
      end)
    end
  end}

  return choices
end

local wammo_weight = function(args)
  return 0.01
end

items["wammo"] = {wammo_name,wammo_desc,wammo_choices,wammo_weight}

return items
