
local cfg = {}

cfg.lang = "en"
-- define customization parts
local parts = {
  ["Face"] = -1,
  ["Blemishes"] = 0,
  ["Beard"] = 1,
  ["Eyebrows"] = 2,
  ["Ageing"] = 3,
  ["Makeup"] = 4,
  ["Blush"] = 5,
  ["Complexion"] = 6,
  ["Skin"] = 7,
  ["Lipstick"] = 8,
  ["Moles/Freckles"] = 9,
  ["Chest Hair"] = 10,
  ["Body Blemishes"] = 11,
  ["Hair"] = 12
}

-- changes prices (any change to the character parts add amount to the total price)
cfg.drawable_change_price = 20
cfg.texture_change_price = 5
cfg.barbershops_title = "Barbershop"

-- skinshops list {parts,x,y,z}
cfg.barbershops = {
{parts,-278.97381591797,6227.9697265625,31.695512771606}
}

return cfg
